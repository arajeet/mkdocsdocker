#!/bin/bash

usage() {
    printf "Usage: $0 [ --codepath <code path>] \n [ --action produce|serve  if not specified it will perform both ] \
   \n [ --distedpath <path where the package live> ] \
  \n [--image <specified image name or else it will try to find arajeet/mkdocker image name] \n"
    exit 2
}

while [ $# -gt 0 ]; do

    if [[ $1 == *"--"* ]]; then
        v="${1/--/}"
        typeset $v="$2"
    fi
    shift

done

if [ -z "$codepath" ] || [ -z "$distedpath" ]; then

    echo " Either  Code Path(--codepath) or   Disted Path (--distedpath) is not provided. So exiting ... "
    usage

fi

# Check if image provided then use that or else use the default one 

if [ -z "$image" ]; then

    container=arajeet/mkdocker:latest
else

    container=$image
fi

produce() {

    docker run -d -v $codepath:/code/mkdocs/ -v $distedpath:/disted $container produce

}

serve() {

    docker run -d -p 8000:8000 -v $distedpath:/disted $container serve
}


# Check if action is provided or else run both produce and serve

if [ -z "$action" ]; then

    echo "No action specified so running both produce and action"
    produce
    serve
else
    if [ "$action" = "serve" ]; then
        serve

    elif [ "$action" = "produce" ]; then
        produce
    else
        echo "Dont Understand the action...So exiting "
        usage
    fi
fi
