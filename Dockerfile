FROM python:3.7-alpine

RUN set - x \
    && apk update\
    && pip install mkdocs

COPY dockerentry.sh / 

EXPOSE 8000

ENTRYPOINT ["/dockerentry.sh"]
CMD ["serve"]
