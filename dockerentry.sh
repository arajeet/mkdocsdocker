#!/bin/sh
action=$1
package=/disted/mkpackage.tar.gz

# Produce action create a package out of the current code. 
# We  have implemented a versioning as well to ensure older disted package are not lost. 
# In future we can implement a backout function to ensure we can fall back to the older code to be picked up mkdocs to serve.
# checks if a tar file exist. if it exist then it renames it with timestamp
# Once the rename is done it create a new package out of the current code.



if [ "$action" = "produce" ]; then
  currenttime=$(date '+%Y-%m-%d-%H:%M:%S')

  cd /code/mkdocs/

  if [ -f "$package" ]; then
    mv $package "$package.$currenttime"
  fi
  
  if [ $? -eq 0 ]; then
     tar -czvf "$package" *
  else
     echo "Archiving of older package failed ...Not Creating a new package as it might overwrite the older disted code"  
  fi    


# Action picks up file with mkpackage.tar.gz to and un-tar it in /var/tmp folder to serve the static pages.

elif [ "$action" = "serve" ]; then
  if [ -f "$package" ]; then
    tar -C /var/tmp/ -xzvf /disted/mkpackage.tar.gz 
    cd /var/tmp
    mkdocs serve --dev-addr=0.0.0.0:8000
  else
    echo "No Package to serve. PLease check !!!!!!!!!!!"

  fi

else
  echo "please specify the command either  mkdocs  produce|serve"

fi
