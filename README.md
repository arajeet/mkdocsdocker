# Project Title
   MKDOCS Package and Serve through Docker 
## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)

## About <a name = "about"></a>

Creating a container of MKDOCs which will package your mkdocs file along with your static and dist it to your own disted location.
Once that is done  it creates a container which serves those files as well.

## Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Please follow the below steps 

```
 a) Clone the code.
 b) Run : docker build -t  arajeet/mkdocker .
    ** if current label is specified the mkdockerize.sh can be used without changing image in the file or else will need to specify --image field.
 c) ./mkdockerize.sh --codepath <root directory of your mkdocs where mkdocs.yml exist> --distedpath <path where packages will be kept> \
                     --action <produce|serve if not specified will package your code ands start mkdocs> --image <if not mentioned will try to pull arajeet/mkdocker >

               
```

### Manually Steps

Docker Command to setup Produce. Please add path for root-code-path for mkdocs and also tar-directory-path. 
Tar-directory path is implemented to hold current packaged version and also the previous version which can be helpful for rollback incase of any issue.

```
    docker run -d -p 8000:8000 \
            -v <root-code-path>:/code/mkdocs/ \
            -v <tar-directory-path>:/disted  \
            <image> produce 
```

Docker Command to setup Serving of static Pages
```
    docker run -p 8000:8000  \
            -v <tar-directory-path>:/disted  \
            <image>  serve
```

End with an example of getting some data out of the system or using it for a little demo.

## Usage <a name = "usage"></a>

Add notes about how to use the system.
